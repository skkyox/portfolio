$(document).ready(function () {
  function getCity() {
    return $('#city').val();
  }
  $('#searchBtn').click(function () {
    var city = getCity();
    var apiKey = '2750ee8df3b97ac3d3416fbf1b638879'
    $.getJSON('http://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=' + apiKey, function (data) {
      if(data.cod === 200 || data.cod === "200"){
        $.getJSON("https://maps.googleapis.com/maps/api/geocode/json?address="+encodeURIComponent(city), function(val) {
          if(val.results.length) {
            var location = val.results[0].geometry.location
            var searchCity = {lat: location.lat, lng: location.lng};
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 11,
              center: searchCity
            });
            var url = 'http://openweathermap.org/img/w/' + data.weather[0].icon + '.png'
            var contentString = '<div id="content">'+
              '<div id="siteNotice">'+
              '</div>'+
                '<h1 id="firstHeading" class="firstHeading">' + city + '</h1>'+
              '<div id="bodyContent">'+
              '<p><b>Température : </b>'+ Math.round((data.main.temp - 273.15) * 10) / 10 + ' °C'+
              '<span><b> / Température mini : </b>'+ Math.round((data.main.temp_min - 273.15) * 10) / 10 + ' °C</span>' +
              '<span><b> / Température max : </b>'+ Math.round((data.main.temp_max - 273.15) * 10) / 10 + ' °C</span>'+
              '</p>'+
              '<p><b>Weather : </b><span id="weather_desc">'+ data.weather[0].description + '</span><img src="' + url + '">' + '</p>' +
              '</div>'+
              '</div>';

            var infowindow = new google.maps.InfoWindow({
              content: contentString
            });

            var marker = new google.maps.Marker({
              position: searchCity,
              map: map,
              title: searchCity.toString()
            });
            marker.addListener('click', function() {
              infowindow.open(map, marker);
            });
          }
        })
      } else {
        alert("Cette ville n'existe pas")
      }
    });
  })
});