<?php

namespace TLS\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class WorkController extends Controller
{
    public function indexAction()
    {
        return $this->render('@TLSCore/Work/work.html.twig');
    }
}
