<?php

namespace TLS\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AboutController extends Controller
{
    public function indexAction()
    {
        return $this->render('@TLSCore/About/about.html.twig');
    }
}
